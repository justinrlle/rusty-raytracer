use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;
use std::ops::Div;

use super::Color;
use image::Rgb;

pub struct Point {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

pub type Vector = Point;

pub struct GeometryForm {
    pub diffusion: f32,
    pub reflection: f32,
    pub spec_coef: f32,
    pub shininess: f32,
    pub color: Color,
    pub refraction: f32,
    pub opacity: f32,
}

pub struct Sphere {
    pub center: Point,
    pub radius: f32,
    pub form: GeometryForm,
}

pub struct ParallelepipedRec {
    pub center: Point,
    pub length: f32,
    pub height: f32,
    pub width: f32,
    pub form: GeometryForm,
}

pub struct Plane {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
    pub form: GeometryForm,
}

pub struct Triangle {
    pub p1: Point,
    pub p2: Point,
    pub p3: Point,
    pub form: GeometryForm,
}

impl ParallelepipedRec {
    pub fn new_cube(center: Point, size: f32, form: GeometryForm) -> ParallelepipedRec {
        ParallelepipedRec {
            center: center,
            length: size,
            height: size,
            width: size,
            form: form,
        }
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, other: Point) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Mul for Point {
    type Output = Point;

    fn mul(self, other: Point) -> Point {
        Point {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z,
        }
    }
}

impl Div for Point {
    type Output = Point;

    fn div(self, other: Point) -> Point {
        Point {
            x: self.x / other.x,
            y: self.y / other.y,
            z: self.z / other.z,
        }
    }
}
