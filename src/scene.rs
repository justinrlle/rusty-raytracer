use super::Color;

use super::geometry::{Point, Vector};

pub struct Screen {
    width: u32,
    height: u32,
}

pub struct Camera {
    pos: Point,
    dir1: Vector,
    dir2: Vector,
    angle: f32,
}

pub enum Light {
    Ambiant { color: Color },
    Directional { color: Color, dir: Vector },
    Point { color: Color, pos: Point },
}

