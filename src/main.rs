extern crate image;

mod geometry;
mod scene;

pub type Color = image::Rgb<u8>;

fn main() {
    println!("Hello, world!");
}
